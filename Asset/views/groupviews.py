# -*- coding: utf-8 -*-
# @Time    : 2020/9/30
# @Author  : canyuan
# @Email   : gy071089@outlook.com
# @File    : groupviews.py

from django.http import JsonResponse
from rest_framework.decorators import api_view
from Base.Functions.basefun import MyPageNumberPagination, xssfilter
from django.views.decorators.csrf import csrf_protect
from .. import models, serializers, forms
from ..Functions import assetfun
from django.db.models import Q


@api_view(['GET'])
def list_views(request):
    data = {
        "code": 0,
        "msg": "",
        "count": '',
        "data": []
    }
    key = request.GET.get('key', '')
    list_get = assetfun.get_group_list(request.user).filter(name__icontains=key).order_by('-update_time')
    list_count = list_get.count()
    pg = MyPageNumberPagination()
    list_page = pg.paginate_queryset(list_get, request, 'self')
    serializers_get = serializers.GroupSerializer(instance=list_page, many=True)
    data['msg'] = 'success'
    data['count'] = list_count
    data['data'] = xssfilter(serializers_get.data)
    return JsonResponse(data)


@api_view(['GET'])
def asset_list_views(request, group_id):
    data = {
        "code": 0,
        "msg": "",
        "count": '',
        "data": []
    }
    key = request.GET.get('key', '')
    group_item = assetfun.check_group_permission(group_id, request.user)
    list_get = group_item.asset.filter(Q(name__icontains=key) | Q(key__icontains=key)).order_by('-update_time')
    list_count = list_get.count()
    pg = MyPageNumberPagination()
    list_page = pg.paginate_queryset(list_get, request, 'self')
    serializers_get = serializers.AssetSerializer(instance=list_page, many=True)
    data['msg'] = 'success'
    data['count'] = list_count
    data['data'] = xssfilter(serializers_get.data)
    return JsonResponse(data)


@api_view(['GET'])
def delete_views(request, group_id):
    data = {
        "code": 1,
        "msg": "",
    }
    item_get = assetfun.check_group_permission(group_id, request.user)
    if item_get:
        item_get.delete()
        data['code'] = 0
        data['msg'] = 'success'
    else:
        data['msg'] = '请检查权限'
    return JsonResponse(data)


@api_view(['POST'])
@csrf_protect
def create_views(request):
    data = {
        "code": 1,
        "msg": "",
    }
    form = forms.GroupForm(request.POST)
    if form.is_valid():
        group_item = models.Group.objects.get_or_create(
            name=form.cleaned_data['name'],
        )
        if group_item[1]:
            group_item = group_item[0]
            group_item.description = form.cleaned_data['description']
            group_item.manage = form.cleaned_data['manage']
            parent_get = form.cleaned_data['parent']
            if parent_get and parent_get != group_item:
                group_item.parent = form.cleaned_data['parent']
            if form.cleaned_data['asset']:
                asset_list = models.Asset.objects.filter(
                    id__in=form.cleaned_data['asset'].split(','))
                group_item.asset.add(*asset_list)
            group_item.save()
            data['code'] = 0
            data['msg'] = 'success'
        else:
            data['msg'] = '分组已存在'
    else:
        data['msg'] = '请检查输入'
    return JsonResponse(data)


@api_view(['POST'])
@csrf_protect
def update_views(request, group_id):
    data = {
        "code": 1,
        "msg": "",
    }
    item_get = assetfun.check_group_permission(group_id, request.user)
    if item_get:
        form = forms.GroupForm(request.POST, instance=item_get)
        if form.is_valid():
            item_get.name = form.cleaned_data['name']
            item_get.description = form.cleaned_data['description']
            item_get.manage = form.cleaned_data['manage']
            parent_get = form.cleaned_data['parent']
            if parent_get and parent_get != item_get:
                item_get.parent = form.cleaned_data['parent']
            if form.cleaned_data['asset']:
                asset_list = models.Asset.objects.filter(
                    id__in=form.cleaned_data['asset'].split(','))
                item_get.asset.add(*asset_list)
            item_get.save()
            data['code'] = 0
            data['msg'] = 'success'
        else:
            data['msg'] = '请检查权限'
    else:
        data['msg'] = '请检查输入'
    return JsonResponse(data)
