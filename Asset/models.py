# coding:utf-8
from django.db import models
from django.contrib.auth.models import User
from Base import models as basemodels


# Create your models here.
# 资产分类，用于区分各资产的属性范围
class Type(models.Model):
    name = models.CharField('分类名称', max_length=50)
    key = models.CharField('分类键值', max_length=50)
    description = models.TextField('分类描述')

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Type'
        verbose_name_plural = '资产分类'


# 资产分类信息，用于定义各类资产所拥有的属性范围
class TypeInfo(models.Model):
    key = models.CharField('属性标识', max_length=30, null=True)
    name = models.CharField('资产属性', max_length=30)
    type_connect = models.ManyToManyField(Type, verbose_name='属性关联', related_name='typeinfo_asset_type', blank=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'TypeInfo'
        verbose_name_plural = '类别属性'


# 资产主体
class Asset(models.Model):
    name = models.CharField('资产名称', max_length=100)
    key = models.CharField('唯一标识', max_length=100)
    type = models.ForeignKey(Type, related_name='type_for_asset', verbose_name='资产类型', on_delete=models.SET_NULL,
                             null=True)
    description = models.TextField('资产说明', null=True, blank=True)

    is_out = models.BooleanField('是否对外', default=False)
    is_use = models.BooleanField('是否有效', default=False)
    weight = models.IntegerField('权重1-n', default=1, blank=True)

    manage = models.ForeignKey(basemodels.Person, related_name='manage_for_asset', verbose_name='负责人', null=True,
                               blank=True, on_delete=models.SET_NULL)

    add_time = models.DateTimeField('添加时间', auto_now_add=True)
    update_time = models.DateTimeField('更新时间', auto_now=True)

    parent = models.ManyToManyField('self', verbose_name='资产关联', related_name='asset_connect',blank=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Asset'
        verbose_name_plural = '资产管理'


# 资产分组，直观体现为业务线
class Group(models.Model):
    name = models.CharField('业务线名称', max_length=100)
    description = models.TextField('业务线描述', null=True,blank=True)
    manage = models.ForeignKey(basemodels.Person, related_name='manage_for_asset_group', verbose_name='负责人', null=True,
                               blank=True, on_delete=models.SET_NULL)
    asset = models.ManyToManyField(Asset,verbose_name='资产关联', related_name='asset_2_group')

    parent = models.ForeignKey('self', verbose_name='分组关联', related_name='group_2_group', null=True, blank=True, on_delete=models.SET_NULL)
    add_time = models.DateTimeField('添加时间', auto_now_add=True)
    update_time = models.DateTimeField('更新时间', auto_now=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Group'
        verbose_name_plural = '业务线'


class OsInfo(models.Model):
    key = models.CharField('IP地址', null=True, blank=True, max_length=100)
    hostname = models.CharField(max_length=50, verbose_name="主机名")
    os = models.CharField(max_length=50, verbose_name="操作系统")
    update_time = models.DateTimeField('更新时间', auto_now=True)

    asset = models.OneToOneField(Asset, related_name='os_for_asset', on_delete=models.CASCADE)

    def __str__(self):
        return self.key

    class Meta:
        verbose_name = 'OsInfo'
        verbose_name_plural = '服务器信息'


class TagInfo(models.Model):
    name = models.CharField('标签名称', max_length=100)
    description = models.TextField('分类描述')
    asset = models.ManyToManyField(Asset, related_name='tag_for_asset', verbose_name='资产关联')

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'TagInfo'
        verbose_name_plural = '标签信息'


class LanguageType(models.Model):
    name = models.CharField('开发语言', max_length=100)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'LanguageType'
        verbose_name_plural = '编程语言'


class WebInfo(models.Model):
    key = models.CharField('域名地址', null=True, blank=True, max_length=100)
    middleware = models.CharField('中间件', max_length=50, blank=True, null=True)
    middleware_version = models.CharField('版本', max_length=50, blank=True, null=True)
    language = models.ForeignKey(LanguageType, verbose_name='编程语言', related_name='type_language', null=True, blank=True,
                                 on_delete=models.SET_NULL)
    web_framwork = models.CharField('开发框架', max_length=50, blank=True, null=True)
    web_framwork_version = models.CharField('开发框架版本', max_length=50, blank=True, null=True)
    update_time = models.DateTimeField('更新时间', auto_now=True)
    asset = models.OneToOneField(Asset, related_name='web_for_asset', on_delete=models.CASCADE)

    def __str__(self):
        return self.key

    class Meta:
        verbose_name = 'WebInfo'
        verbose_name_plural = '网站信息'


class PortInfo(models.Model):
    port = models.IntegerField('开放端口')
    protocol = models.CharField('端口类型', max_length=50, null=True, blank=True)
    name = models.CharField('服务名称', max_length=50, null=True, blank=True)
    product = models.CharField('产品信息', max_length=100, null=True, blank=True)
    version = models.CharField('应用版本', max_length=50, null=True, blank=True)
    port_info = models.TextField('端口介绍', null=True, blank=True)
    update_time = models.DateTimeField('更新时间', auto_now=True)
    is_new = models.BooleanField('是否新端口', default=True)

    asset = models.ForeignKey(Asset, related_name='port_for_asset', on_delete=models.CASCADE)

    def __str__(self):
        return self.port

    class Meta:
        verbose_name = 'PortInfo'
        verbose_name_plural = '端口信息'


class PluginInfo(models.Model):
    name = models.CharField('组件名称', max_length=50)
    version = models.CharField('应用版本', max_length=50, null=True)
    plugin_info = models.TextField('组件简介', null=True)
    create_time = models.DateTimeField('添加时间', auto_now_add=True)
    update_time = models.DateTimeField('更新时间', auto_now=True)
    is_delete = models.BooleanField('是否删除', default=False)

    asset = models.ForeignKey(Asset, related_name='plugin_for_asset', on_delete=models.CASCADE)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'PluginInfo'
        verbose_name_plural = '组件信息'

