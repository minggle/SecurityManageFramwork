# -*- coding: utf-8 -*-
# @Time    : 2020/7/10
# @Author  : canyuan
# @Email   : gy071089@outlook.com
# @File    : policiesviews.py
from django.http import JsonResponse
from rest_framework.decorators import api_view
from django.views.decorators.csrf import csrf_protect
from .. import forms, models


@api_view(['POST'])
@csrf_protect
def policies_create(request, scanner_id):
    data = {
        "code": 1,
        "msg": "",
        "data": []
    }
    scanner_item = models.Scanner.objects.filter(id=scanner_id).first()
    if scanner_item:
        form = forms.PoliciesEditForm(request.POST)
        if form.is_valid():
            models.Policies.objects.get_or_create(
                name=form.cleaned_data['name'],
                key=form.cleaned_data['key'],
                description=form.cleaned_data['description'],
                scanner=scanner_item
            )
            data['code'] = 0
            data['msg'] = 'success'
        else:
            data['msg'] = '请检查参数'
    else:
        data['msg'] = '你要干啥'
    return JsonResponse(data)


@api_view(['POST'])
@csrf_protect
def policies_update(request, police_id):
    data = {
        "code": 1,
        "msg": "",
        "data": []
    }
    police_item = models.Policies.objects.filter(id=police_id).first()
    if police_item:
        form = forms.PoliciesEditForm(request.POST,instance=police_item)
        if form.is_valid():
            form.save()
            data['code'] = 0
            data['msg'] = 'success'
        else:
            data['msg'] = '请检查参数'
    else:
        data['msg'] = '你要干啥'
    return JsonResponse(data)


@api_view(['GET'])
@csrf_protect
def policies_delete(request, police_id):
    data = {
        "code": 1,
        "msg": "",
        "data": []
    }
    police_item = models.Policies.objects.filter(id=police_id).first()
    if police_item:
        police_item.delete()
        data['code'] = 0
        data['msg'] = 'success'
    else:
        data['msg'] = '你要干啥'
    return JsonResponse(data)