# -*- coding: utf-8 -*-
# @Time    : 2020/6/16
# @Author  : canyuan
# @Email   : gy071089@outlook.com
# @File    : urls.py

from django.urls import path
from .views import views, pocviews

urlpatterns = [
    path('list/', views.mainlist, name='main_list'),
    path('listcheck/', views.checklist, name='checklist'),
    path('create/', pocviews.poc_create, name='poc_create'),
    path('delete/<str:poc_id>/', pocviews.poc_delete, name='poc_delete'),
    path('update/<str:poc_id>/', pocviews.poc_update, name='poc_update'),
    path('views/<str:poc_id>/', pocviews.poc_details, name='poc_details'),
    path('test/<str:poc_id>/', pocviews.poc_test, name='poc_test'),
    path('check/<str:poc_id>/', pocviews.poc_check, name='poc_check'),
]