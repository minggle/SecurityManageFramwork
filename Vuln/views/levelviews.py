# -*- coding: utf-8 -*-
# @Time    : 2020/11/10
# @Author  : canyuan
# @Email   : gy071089@outlook.com
# @File    : levelviews.py

from django.http import JsonResponse
from rest_framework.decorators import api_view
from Base.Functions.basefun import xssfilter
from .. import models, serializers


@api_view(['GET'])
def select_level_views(request):
    data = {
        "code": 0,
        "msg": "",
        "count": '',
        "data": []
    }
    list_get = models.LEVEL.objects.all()
    list_count = list_get.count()
    serializers_get = serializers.LEVELSelectSerializer(instance=list_get, many=True)
    data['msg'] = 'success'
    data['count'] = list_count
    data['data'] = xssfilter(serializers_get.data)
    return JsonResponse(data)