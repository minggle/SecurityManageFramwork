# coding:utf-8
from django.contrib import admin
from . import models
# Register your models here.


admin.site.register(models.Type)
admin.site.register(models.LEVEL)
admin.site.register(models.Vuln)
admin.site.register(models.Source)
