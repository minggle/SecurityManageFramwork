# -*- coding: utf-8 -*-
# @Time    : 2020/6/24
# @Author  : canyuan
# @Email   : gy071089@outlook.com
# @File    : serializers.py
from rest_framework import serializers
from . import models


class DepartmentSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Department
        fields = ('id', 'name', 'parent_id')


class AreaSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Area
        fields = ('id', 'name', 'description')


class PersonSerializer(serializers.ModelSerializer):
    dep = serializers.SerializerMethodField('get_dep')

    class Meta:
        model = models.Person
        fields = ('id', 'name', 'mail', 'dep', 'is_valid')

    def get_dep(self, obj):
        try:
            return {'id': obj.dep.id, 'name': obj.dep.name}
        except:
            return None


class PersonSelectSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Person
        fields = ('id', 'name', 'mail')


class PermissionManageSerializer(serializers.ModelSerializer):
    department = serializers.SerializerMethodField('get_department')
    area = serializers.SerializerMethodField('get_area')

    class Meta:
        model = models.PermissionManage
        fields = ('id', 'department', 'area', 'description')

    def get_department(self, obj):
        try:
            return {'id':obj.department.id, 'name':obj.department.name}
        except:
            return None

    def get_area(self, obj):
        try:
            return {'id':obj.area.id,'name':obj.area.name}
        except:
            return None


class ClientSerializer(serializers.ModelSerializer):
    person = serializers.SerializerMethodField('get_person')

    class Meta:
        model = models.Client
        fields = "__all__"

    def get_person(self, obj):
        try:
            return {'id': obj.person.id, 'name': obj.person.name}
        except:
            return None


class DomainSerializer(serializers.ModelSerializer):
    manage = serializers.SerializerMethodField('get_manage')
    start_date = serializers.DateField(format='%Y-%m-%d')
    end_date = serializers.DateField(format='%Y-%m-%d')
    create_time = serializers.DateTimeField(format='%Y-%m-%d %H:%M')
    update_time = serializers.DateTimeField(format='%Y-%m-%d %H:%M')

    class Meta:
        model = models.Domain
        fields = ('id', 'key', 'description', 'manage', 'start_date', 'end_date', 'create_time', 'update_time')

    def get_manage(self, obj):
        try:
            return {'id': obj.manage.id, 'name': obj.manage.name}
        except:
            return None


class VlanSerializer(serializers.ModelSerializer):
    manage = serializers.SerializerMethodField('get_manage')
    createtime = serializers.DateTimeField(format='%Y-%m-%d %H:%M')
    updatetime = serializers.DateTimeField(format='%Y-%m-%d %H:%M')

    class Meta:
        model = models.Vlan
        fields = ('id', 'key', 'description', 'manage', 'createtime', 'updatetime')

    def get_manage(self, obj):
        try:
            return {'id': obj.manage.id, 'name': obj.manage.name}
        except:
            return None


class UsernameSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Username
        fields = ('id', 'name', 'count')


class U_passwdSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.U_Passwd
        fields = ('id', 'name', 'count')