# coding:utf-8

from django.db import models
from django.contrib.auth.models import User


# Create your models here.
class Area(models.Model):
    name = models.CharField('区域名称', max_length=20)
    description = models.TextField('备注说明', null=True, blank=True)
    # parent = models.ForeignKey('self', verbose_name='上级区域', related_name='area_to_area', null=True, blank=True, on_delete=models.CASCADE)

    def __str__(self):
        # 显示层级菜单
        return self.name

    class Meta:
        verbose_name = 'Area'
        verbose_name_plural = '区域划分'


class Department(models.Model):
    name = models.CharField('部门名称', max_length=20)
    parent = models.ForeignKey('self', verbose_name='上级部门', related_name='dep_to_dep', null=True, blank=True,
                               on_delete=models.CASCADE)

    def __str__(self):
        # 显示层级菜单
        title_list = [self.name]
        p = self.parent
        while p:
            title_list.insert(0, p.name)
            p = p.parent
        return '-'.join(title_list)

    class Meta:
        verbose_name = 'Department'
        verbose_name_plural = '部门管理'


class PermissionManage(models.Model):
    department = models.ForeignKey(Department, verbose_name='负责部门', related_name='profession_to_permission', on_delete=models.CASCADE)
    area = models.ForeignKey(Area, verbose_name='管理区域', related_name='area_to_permission', null=True, blank=True,
                             on_delete=models.CASCADE)
    description = models.TextField('备注说明', null=True, blank=True)

    def __str__(self):
        return str(self.area.name) + '-' + str(self.department.name)

    class Meta:
        verbose_name = 'PermissionManage'
        verbose_name_plural = '管理权限'


class Person(models.Model):
    name = models.CharField('员工名称', max_length=20)
    is_valid = models.BooleanField('是否在职', default=True)
    mail = models.EmailField('邮箱地址')
    phone = models.CharField('联系电话', max_length=50, null=True,blank=True)
    dep = models.ForeignKey(Department, verbose_name='所属部门', related_name='dep_for_person', null=True, blank=True,
                              on_delete=models.SET_NULL)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Person'
        verbose_name_plural = '员工信息'


class Client(models.Model):
    ip = models.CharField('IP地址',max_length=30)
    mac = models.CharField('MAC地址',max_length=40)
    des = models.TextField('备注')
    person = models.ForeignKey(Person,verbose_name='终端用户', related_name='person_to_client', null=True, on_delete=models.SET_NULL)
    updatetime = models.DateTimeField('更新时间', auto_now=True)

    def __str__(self):
        return str(self.id)

    class Meta:
        verbose_name = 'Client'
        verbose_name_plural = '终端信息'


class Domain(models.Model):
    key = models.CharField('域名', max_length=100, unique=True)
    description = models.TextField('域名备注', null=True, blank=True)
    manage = models.ForeignKey(Person, related_name='manage_for_domain', null=True, blank=True, on_delete=models.SET_NULL)

    start_date = models.DateField('生效时间')
    end_date = models.DateField('失效事件')

    create_time = models.DateTimeField('收录时间', auto_now_add=True)
    update_time = models.DateTimeField('更新时间', auto_now=True)

    def __str__(self):
        return self.key

    class Meta:
        verbose_name = 'Domain'
        verbose_name_plural = '域名资产'


class Vlan(models.Model):
    key = models.CharField('网段', max_length=100, unique=True)
    description = models.TextField('网段备注', null=True, blank=True)
    manage = models.ForeignKey(Person, related_name='manage_for_vlan', null=True, blank=True, on_delete=models.SET_NULL)

    createtime = models.DateTimeField('收录时间', auto_now_add=True)
    updatetime = models.DateTimeField('更新时间', auto_now=True)

    def __str__(self):
        return self.key

    class Meta:
        verbose_name = 'Domain'
        verbose_name_plural = '内网网段'


class Username(models.Model):
    name = models.CharField('用户名', max_length=50)
    count = models.IntegerField(default=0)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Username'
        verbose_name_plural = '常用账号'


class U_Passwd(models.Model):
    name = models.CharField('用户名', max_length=50)
    count = models.IntegerField(default=0)
    username = models.ManyToManyField(Username, verbose_name='用户名关联', related_name='u_passwd_2_username')

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'U_Passwd'
        verbose_name_plural = '常用密码'

