# -*- coding: utf-8 -*-
# @Time    : 2020/5/12
# @Author  : canyuan
# @Email   : gy071089@outlook.com
# @File    : urls.py

from django.urls import path
from .views import views

urlpatterns = [
    path('loglist/', views.Log_list, name='loglist'),
    path('mainlist/', views.mainlist, name='mainlist'),
]
