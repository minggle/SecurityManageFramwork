# coding:utf-8
from django.http import JsonResponse
from ..Functions.jwttoken import MyTokenObtainPairSerializer
from rest_framework.permissions import AllowAny
import django.utils.timezone as timezone
from rest_framework.decorators import api_view, permission_classes
from django.contrib.auth.models import User
from django.contrib import auth
from .. import forms, models, serializers
import datetime
from django.utils.html import escape
from django.views.decorators.csrf import csrf_protect
from rest_framework_simplejwt.tokens import RefreshToken
from ..Functions import menu
from Log.Functions import logfun
from Base.Functions import basefun
from Base.Functions import getuserip
from Base.Functions.basefun import xssfilter

cur_date = datetime.datetime.now().date()


# Create your views here.


@api_view(['POST'])
@csrf_protect
@permission_classes((AllowAny,))
def login(request):
    json_data = {
        "code": 1
        , "msg": ""
        , "data": {
        }
    }
    log = {
        'type': '账号操作',
        'user': '',
        'ip': getuserip.get_client_ip(request),
        'action': '用户登陆',
        'status': False,
    }
    form = forms.LoginForm(request.POST)
    if form.is_valid():
        username = form.cleaned_data['username']
        password = form.cleaned_data['password']
        user_get = User.objects.filter(username=username).first()
        if user_get:
            log['user'] = user_get
            if user_get.profile.lock_time > timezone.now():
                json_data['msg'] = u'账号已锁定,' + str(user_get.profile.lock_time.strftime("%Y-%m-%d %H:%M")) + '后可尝试'
            else:
                user = auth.authenticate(username=username, password=password)
                if user:
                    user.profile.error_count = 0
                    user.profile.login_count += 1
                    user.save()
                    token = MyTokenObtainPairSerializer.get_token(user)
                    json_data['code'] = 0
                    json_data['msg'] = '登陆成功'
                    json_data['data']['access_token'] = token
                    json_data['data']['deny'] = user.is_superuser
                    json_data['data']['is_resetpsd'] = user.profile.is_resetpsd
                    log['status'] = True
                else:
                    user_get.profile.error_count += 1
                    if user_get.profile.error_count >= 5:
                        user_get.profile.lock_time = timezone.now() + datetime.timedelta(minutes=10)
                    user_get.save()
                    json_data['msg'] = '登陆失败,已错误登录' + str(user_get.profile.error_count) + '次,5次后账号锁定'
            log['action'] = log['action'] + json_data['msg']
        else:
            json_data['msg'] = '请检查登录信息'
    else:
        json_data['msg'] = '验证码错误'
    if log['user']:
        logfun.createLog(log)
    return JsonResponse(json_data)


@api_view(['GET'])
def logout(request):
    token = request.META.get("HTTP_AUTHORIZATION").split(' ')[1]
    token = RefreshToken(token)
    token.blacklist()
    json_data = {"code": 1,
                 'msg': '注销成功',
                 'data': []
                 }
    return JsonResponse(json_data)


@api_view(['GET'])
def selfinfo(request):
    user = request.user
    data = {
        "code": 0
        , "msg": "self information"
        , "data": []
    }
    info = serializers.UserSerializer(instance=user)
    data['data'] = xssfilter(info.data)
    return JsonResponse(data)


@api_view(['GET'])
def session(request):
    user = request.user
    data = {
        "code": 0
        , "msg": "self information"
        , "data": {
            "username": escape(user.username),
            'is_superuser': user.is_superuser,
            'is_resetpsd': user.profile.is_resetpsd,
        }
    }
    return JsonResponse(data)


@api_view(['POST'])
@csrf_protect
def selfupdate(request):
    data = {
        "code": 1,
        "msg": "",
        "data": []
    }
    user = request.user
    form = forms.ProfileUpdateForm(request.POST)
    if form.is_valid():
        user.profile.title = form.cleaned_data['title']
        user.profile.manage = form.cleaned_data['manage']
        user.profile.phone = form.cleaned_data['phone']
        user.profile.mail = form.cleaned_data['mail']
        user.email = form.cleaned_data['mail']
        user.save()
        data['code'] = 0
        data['msg'] = '更新成功'
    else:
        data['msg'] = '请检查参数'
    return JsonResponse(data)


@api_view(['GET'])
def get_menu(request):
    user = request.user
    json_data = {
        "code": 0
        , "msg": ""
        , "data": {
        }
    }
    role_list = models.Role.objects.none()
    # menu_list = models.Menu.objects.none()
    if not user.is_superuser:
        menu_list = user.profile.roles.menu.all()
    else:
        menu_list = models.Menu.objects.filter(parent__isnull=True)
    menu_list = menu_list.order_by('order')
    if not menu_list and not role_list:
        json_data['code'] = 1
        json_data['msg'] = '无可访问的资源或菜单初始化失败,请联系管理员'
        json_data['data'] = [{
            "name": "主页"
            , "icon": "layui-icon-home"
            , "subMenus": [{
                "name": "控制台"
                , "url": "/base/index/"
            }]
        }]
    else:
        # json_data['data'] = menu.menutotree(menu_list, True)
        # json_data['data'] = menu.menutotree_easyweb(menu_list, True)
        json_data['data'] = menu.menutotree_vue(menu_list, True)
    return JsonResponse(json_data)


@api_view(['POST'])
@csrf_protect
def resetpsd(request):
    user = request.user
    data = {
        "code": 1
        , "msg": ""
        , "data": {
            'status': 'false'
            , 'token': ''}
    }
    log = {
        'type': '账号操作',
        'user': user,
        'ip': getuserip.get_client_ip(request),
        'action': '修改密码',
        'status': False,
    }
    if request.method == 'POST':
        form = forms.ResetpsdForm(request.POST)
        if form.is_valid():
            oldpassword = form.cleaned_data['oldpassword']
            newpassword = form.cleaned_data['newpassword']
            repassword = form.cleaned_data['repassword']
            if basefun.checkpsd(newpassword):
                if newpassword and newpassword == repassword:
                    if oldpassword:
                        user = auth.authenticate(username=user.username, password=oldpassword)
                        if user:
                            user.set_password(newpassword)
                            user.save()
                            auth.logout(request)
                            data['code'] = 0
                            data['data']['status'] = 'true'
                            data['msg'] = '重置成功，请重新登陆'
                            log['status'] = True
                        else:
                            data['msg'] = '请检查原始密码'
                    else:
                        data['msg'] = '请检查原始密码'
                else:
                    data['msg'] = '两次密码不一致'
            else:
                data['msg'] = '密码不符合安全规范'
            logfun.createLog(log)
        else:
            data['msg'] = '请检查输入'
    else:
        data['msg'] = '请求方式错误'
    return JsonResponse(data, safe=False)
